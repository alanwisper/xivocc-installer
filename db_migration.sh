#!/bin/bash

set -e

OLD_DB="-h localhost -p ${OLD_DB_PORT}"
NEW_DB="-h localhost -p ${NEW_DB_PORT}"


is_db_running() {
    result=$(psql $1 --quiet -tAc "SELECT 1 FROM pg_catalog.pg_database WHERE datname='$2'" 2> /dev/null)

    [ $? -eq 0 ] && [ "$result" = "1" ]
}

create_pgpass_file() {
    echo "localhost:5432:asterisk:asterisk:${POSTGRES_PASSWORD}" > /home/postgres/.pgpass
    echo "localhost:5433:postgres:postgres:${POSTGRES_PASSWORD}" >> /home/postgres/.pgpass
    chmod 600 /home/postgres/.pgpass
}

run_migration_to_pg_11() {
    pg_dump ${OLD_DB} -U asterisk -Fc asterisk \
    | pg_restore --verbose ${NEW_DB} --clean --create -d postgres
}

echo "Running database migration container..."

create_pgpass_file

if ! is_db_running "${OLD_DB} -U asterisk" asterisk; then
    echo "Can't connect to the current database on port ${OLD_DB_PORT}."
    exit 2
fi
if ! is_db_running "${NEW_DB} -U postgres" postgres; then
    echo "Can't connect to the new database on port ${NEW_DB_PORT}."
    exit 3
fi

run_migration_to_pg_11
