#!/bin/bash
set -e

DB_HOST="localhost"
DB_PORT=5443
DB_MIGRATION_VERSION="2019.05"
FACTORY_ENV_FILE="/etc/docker/compose/factory.env"
PG_DATA_FOLDER="/var/lib/postgresql"
PG_11_DATA="$PG_DATA_FOLDER/11"

REQUIRED_SPACE=450 #in megabytes

locale_fr="fr_FR.utf8"
db_data_folder_old="$PG_DATA_FOLDER/data/"
db_data_folder_new="$PG_DATA_FOLDER/9.4/"
postgres_initdb_args="--encoding UTF8 --lc-collate ${locale_fr} --lc-ctype ${locale_fr} --lc-time ${locale_fr}"



is_postgres_running() {
    result=$(PGPASSWORD=xivocc psql -h "$DB_HOST" -p $DB_PORT -U postgres -qAtc "select 1 from pg_catalog.pg_database WHERE datname='postgres'" 2>/dev/null)

    [ $? -eq 0 ] && [ "$result" = "1" ]
}

wait_for_postgres() {
    local wait_time=120
    echo -n "Waiting $wait_time seconds for database pgxivocc..."

    local db_running="false"
    for _ in $(seq 1 $wait_time); do
        if is_postgres_running; then
            db_running="true"
            break
        else
            sleep 1
            echo -n "."
        fi
    done
    echo
    if [ "$db_running" = "false" ]; then
        echo "ERROR: database is not running."
        exit 1
    fi
}

properly_shutdown_database() {
    xivocc-dcomp start pgxivocc
    wait_for_postgres
    xivocc-dcomp kill -s SIGINT pgxivocc
}

move_data_folder() {
    mv $db_data_folder_old $db_data_folder_new
}

get_distribution() {
    local distribution
    distribution=$(grep -oP -m 1 'XIVOCC_DIST=\K.*' ${FACTORY_ENV_FILE})
    echo "$distribution"
}

migrate_94_to_11() {
    set -o pipefail
    distribution=${1}
    # Note: Using implicit parameter from Dockerfile: PGDATANEW /var/lib/postgresql/11/main
    docker run --rm \
        -v /etc/timezone:/etc/timezone:ro \
        -v /etc/localtime:/etc/localtime:ro \
        -v /var/lib/postgresql/:/var/lib/postgresql/ \
        -e LANG="${locale_fr}" \
        -e POSTGRES_INITDB_ARGS="${postgres_initdb_args}" \
        -e PGDATAOLD="$db_data_folder_new" \
        xivoxc/xivo-db-migration:${DB_MIGRATION_VERSION}."${distribution}" --link 2>&1
}

display_upgrade_notice() {
    echo "*********************************************************************************"
    echo "*   XiVO-CC Database was upgraded from Postgres version 9.4 to version 11       *"
    echo "*********************************************************************************"
}

clean_pg_upgrade_remainders() {
    # Remove old cluster and clean pg_upgrade files
    rm -rf "$db_data_folder_new" \
        "$db_data_folder_old" \
        /var/lib/postgresql/analyze_new_cluster.sh \
        /var/lib/postgresql/delete_old_cluster.sh
}

add_access_configuration() {
    {
        echo
        echo "host all all 0.0.0.0/0 md5"
    } >>"$PG_11_DATA/main/pg_hba.conf"
}

check_available_disk_space() {
    AVAILABLE_SPACE=$(df --output='avail' -m $PG_DATA_FOLDER | tail -n +2)
    if [ "$AVAILABLE_SPACE" -lt "$REQUIRED_SPACE" ]; then
        echo "$(tput bold)$(tput setaf 3)To migrate the database, more disk space is required, available: $AVAILABLE_SPACE MB, required: $REQUIRED_SPACE MB$(tput sgr 0)"
        echo "$(tput bold)$(tput setaf 3)Please make more space on the disk$(tput sgr 0)"
        exit 1
    fi
}

migrate() {
    local distribution
    echo "Starting database migration to postgres 11..."

    echo "Re-starting database with fast shutdown..."

    properly_shutdown_database
    if is_postgres_running; then
        echo "ERROR: Failed to stop database."
        exit 3
    fi

    echo "Pulling docker migration container..."
    distribution=$(get_distribution)
    docker pull xivoxc/xivo-db-migration:${DB_MIGRATION_VERSION}."${distribution}"

    echo "Moving postgres data folder..."
    move_data_folder

    echo "Running migration..."
    migrate_94_to_11 "$distribution"

    if [ "$?" -eq 0 ]; then
        # Script MUST exit on error even if "set -e" was removed
        echo "Database upgrade successful."
        set +o pipefail

        clean_pg_upgrade_remainders

        echo "Add allow connections from all..."
        add_access_configuration

        display_upgrade_notice
    fi
}

if [ ! -d "$PG_11_DATA" ]; then
    check_available_disk_space
    migrate
else
    echo "ERROR: $PG_11_DATA directory exists."
fi

exit 0
