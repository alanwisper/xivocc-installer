#!/bin/bash
# Copyright (C) 2016 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

XIVOCC_HOST=""
RESTART_REPLY="false"
XIVO_AMI_SECRET=""
INSTALLATION_TYPE=""
COMPOSE_PATH="/etc/docker/compose"
COMPOSE_PATH_XIVO="/etc/docker/xivo"
FACTORY_ENV_FILE="factory.env"
CUSTOM_ENV_FILE="custom.env"
XIVOXC_ENABLED_FILE="/var/lib/xivo/xc_enabled"
XIVO_MONITORING_UPDATE=/usr/sbin/xivo-monitoring-update
POSTGRES_PATH=/var/lib/postgresql/11/main
POSTGRES_CONF_FILE=$POSTGRES_PATH/postgresql.conf
PG_HBA_CONF_FILE=$POSTGRES_PATH/pg_hba.conf


check_args() {
    if [[ "$XIVOCC_HOST" = "" ]] || [[ "$INSTALLATION_TYPE" = "" ]]; then
        cat <<-EOF
		Missing args.
		Script to be run only by xivocc-installer or xivouc-installer.
		Exiting.
		EOF
        exit 0
    fi
}

update_postgresql_config_file() {
    echo -e "\e[1;32mXiVO PBX editing postgresql.conf\e[0m"
    sed -i.bak '/listen_addresses/s/localhost/*/' $POSTGRES_CONF_FILE
    sed -i.bak '/listen_addresses/s/#listen_addresses/listen_addresses/' $POSTGRES_CONF_FILE
    if [ "$INSTALLATION_TYPE" != "UC" ]; then
        echo -e "\e[1;32mXiVO PBX adding this entry for xuc connection to pg_hba.conf:\e[0m"
        local entry="host  asterisk    all $XIVOCC_HOST/32  md5"
        echo "$entry"
        echo "$entry" >> $PG_HBA_CONF_FILE
        echo -e "\e[1;33mPlease check that xuc uses this address for connection to XiVO database\e[0m"
        echo -e "\e[1;33mor edit this entry in $PG_HBA_CONF_FILE\e[0m"
    fi
}

reload_asterisk() {
    echo -e "\e[1;32mXiVO PBX Reloading Asterisk Manager\e[0m"
    asterisk -rx "manager reload"
    echo -e "\e[1;32mXiVO PBX Verify configuration\e[0m"
    asterisk -rx "manager show user xuc"

}
update_asterisk_config() {
    touch /etc/asterisk/manager.d/02-xivocc.conf
    echo -e "\e[1;32mXiVO PBX Creating 02-xivocc.conf\e[0m"
    echo "
[xuc]
secret = $XIVO_AMI_SECRET
deny=0.0.0.0/0.0.0.0
permit=$XIVOCC_HOST/255.255.255.255" > /etc/asterisk/manager.d/02-xivocc.conf
    if [ "$INSTALLATION_TYPE" = "UC" ]; then
        DOCKER_NET_XUC_IP=$(grep -oP -m 1 'DOCKER_NET_XUC_IP=\K.*' ${COMPOSE_PATH}/${FACTORY_ENV_FILE})
        echo "permit=$DOCKER_NET_XUC_IP/255.255.255.255" >> /etc/asterisk/manager.d/02-xivocc.conf
    fi
    echo "read = system,call,log,verbose,command,agent,user,dtmf,originate,dialplan
write = system,call,log,verbose,command,agent,user,dtmf,originate,dialplan
writetimeout = 10000
" >> /etc/asterisk/manager.d/02-xivocc.conf

    reload_asterisk
}

add_xuc_to_database() {
    cd /tmp
    echo -e "\e[1;32mXiVO PBX Inserting values into database\e[0m"
    echo -e "\e[1;32mXiVO PBX Creating web service\e[0m"
    sudo -u asterisk psql asterisk -c "INSERT INTO accesswebservice(name,login,passwd,host,description) VALUES('xivows','xivows','','${XIVOCC_HOST}','XiVO-CC WS')"
}

add_dbreplic_container() {
  mkdir -p /var/log/xivo-db-replication
  chown -R daemon:daemon /var/log/xivo-db-replication

  REPORTING_DB_HOST_FROM_CUSTOM_ENV=$(grep -oP -m 1 'REPORTING_DB_HOST=\K.*' ${COMPOSE_PATH_XIVO}/${CUSTOM_ENV_FILE})
  if [ -z $REPORTING_DB_HOST_FROM_CUSTOM_ENV ]; then
    if [ "$INSTALLATION_TYPE" = "UC" ]; then
        echo "REPORTING_DB_HOST=172.18.0.1" >> ${COMPOSE_PATH_XIVO}/${CUSTOM_ENV_FILE}
    else
        echo "REPORTING_DB_HOST=${XIVOCC_HOST}" >> ${COMPOSE_PATH_XIVO}/${CUSTOM_ENV_FILE}
    fi
  fi
  ELASTICSEARCH_FROM_CUSTOM_ENV=$(grep -oP -m 1 'ELASTICSEARCH=\K.*' ${COMPOSE_PATH_XIVO}/${CUSTOM_ENV_FILE})
  if [ -z $ELASTICSEARCH_FROM_CUSTOM_ENV ]; then
    echo "ELASTICSEARCH=${XIVOCC_HOST}" >> ${COMPOSE_PATH_XIVO}/${CUSTOM_ENV_FILE}
  fi

  # Enable monit for new service
  ${XIVO_MONITORING_UPDATE}
}

update_host_in_ws_access() {
  XUC_IP_ADDRESS=$1
  sudo -u asterisk psql asterisk -c "UPDATE accesswebservice SET host='${XIVOCC_HOST}' WHERE name='xivows'"
}


while getopts h:r:s:t: opt
do
    case ${opt} in
        h) XIVOCC_HOST=${OPTARG};;
        r) RESTART_REPLY=${OPTARG};;
        s) XIVO_AMI_SECRET=${OPTARG};;
        t) INSTALLATION_TYPE=${OPTARG};;
        '?')
            echo "${0} : option ${OPTARG} is not valid" >&2
            usage
            exit -1
        ;;
    esac
done

check_args

if [[ "$INSTALLATION_TYPE" = "ADD_DBREPLIC" ]]; then
  add_dbreplic_container
elif [[ "$INSTALLATION_TYPE" = "UPDATE_WS_HOST" ]]; then
  update_host_in_ws_access ${XIVOCC_HOST}
else
  # Edit postgresql configuration files
  update_postgresql_config_file

  # Asterisk configuration
  update_asterisk_config

  # Add XUC specific user and websocket service to the database
  add_xuc_to_database

  # Configure dbreplic for docker and monit
  add_dbreplic_container
fi

# Flag that XiVO is plugged with UC or CC
touch ${XIVOXC_ENABLED_FILE}

# Restart XiVO PBX if answer was yes
echo -e "\e[1;32mXiVO PBX restarting services\e[0m"
if [[ "$RESTART_REPLY" = true ]]; then
    echo -e "\e[1;32mXiVO PBX is going to restart NOW\e[0m"
    sleep 3s
    xivo-service restart all
else
    echo -e "\e[1;33mPlease restart XiVO PBX later by xivo-service restart all\e[0m"
fi
echo -e "\e[1;32mXiVO PBX Configuration Done. Thank you.\e[0m"
