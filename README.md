# xivocc-installer

## Manual Building

Before building, you must check you have the necessaries built tools:

```
sudo apt-get install devscripts debhelper
```

Then to manually build the package, you need to issue the following command:

```
debuild -us -uc
```

It will generate the deb file in the parent folder of the current working directory.
On the test environment, you can then install the package manually:

```
dpkg -i xivocc-installer_2017.01_all.deb
```

## Manual Test

To test this repository, you need to clone the `xivo-installer-script` and the `xivocc-installer-helper`.

inside the file  `xivo-install-script/xivocc_install.sh` you have to comment the lines :
```bash
install_xivocc() {
...

${download} xivocc-installer <== COMMENT
${install} xivocc-installer  <== COMMENT

...
}
```

From your future Xivocc machine, create the ssh key and upload it on the xivo :

```BASH
ssh-keygen -t rsa -P "" -f ~/.ssh/xivocc_rsa
```
```BASH
ssh-copy-id -i ~/.ssh/xivocc_rsa root@XIVO_HOST
```

run the `xivo-install-script/xivocc_install.sh`

```bash
 xivo-install-script/xivocc_install.sh -a VERSION-NUMBER
```

Build manually and install the `xivocc-installer-helper`.
Build manually and install this package.

## Silent installation Test

```bash
export DEBIAN_FRONTEND=noninteractive && dpkg -i xivocc-installer_NUMBER
```

