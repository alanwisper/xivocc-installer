FROM tianon/postgres-upgrade:9.4-to-11

# Add fr_FR.UTF-8 to handle upgrade of database in this locale
RUN localedef -i fr_FR -c -f UTF-8 -A /usr/share/locale/locale.alias fr_FR.UTF-8

# Change default to be able to enable the --link option
ENV PGDATAOLD /var/lib/postgresql/9.4/main
ENV PGDATANEW /var/lib/postgresql/11/main
